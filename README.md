# Othello_85



Description:

Othello is a game played by two people on an 8 x 8 board, using disks that are white on one side and black on the other.
On a grid, a black and white players places tile sof their color on the board.The opponent's tiles between the newly placed tile and that player's existing tiles are flipped to become the color of the player's tiles.
The game ends when the board fills up and the player with the most tiles of their color wins.




Input:

1.	The first line of the input is the number of games to be processed. Each game consists of a board configuration followed by a list of commands. The board configuration consists of 9 lines. The first 8 specify the current state of the board. Each of these 8 lines contains 8 characters, and each of these characters will be one of the following:
1.	‘-’ indicating an unoccupied square
2.	‘B’ indicating a square occupied by a black disk
3.	‘W’ indicating a square occupied by a white disk
2.	The ninth line is either a ‘B’ or a ‘W’ to indicate which is the current player. You may assume that the data is legally formatted.
3.	Then a set of commands follows. The commands are to list all possible moves for the current player, make a move, or quit the current game. There is one command per line with no blanks in the input.



Output:

1.	The commands and the corresponding outputs are formatted as follows:
2.	List all possible moves for the current player. The command is an ‘L’ in the first column of the line. The program should go through the board and print all legal moves for the current player in the format (x, y) where x represents the row of the legal move and y represents its column.
3.	These moves should be printed in row major order which means:

1.	all legal moves in row number i will be printed before any legal move in row number j if j is greater than i and
2.	if there is more than one legal move in row number i, the moves will be printed in ascending order based on column number.

1.	All legal moves should be put on one line. If there is no legal move because it is impossible for the current player to bracket any pieces, the program should print the message “No legal move.”
2.	Make a move. The command is an ‘M’ in the first column of the line, followed by 2 digits in the second and third column of the line. The digits are the row and the column of the space to place the piece of the current player’s color, unless the current player has no legal move. If the current player has no legal move, the current player is first changed to the other player and the move will be the move of the new current player.
3.	You may assume that the move is then legal. You should record the changes to the board, including adding the new piece and changing the color of all bracketed pieces. At the end of the move, print the number of pieces of each color on the board in the format “Black - xx White - yy” where xx is the number of black pieces on the board and yy is the number of white pieces on the board.
4.	After a move, the current player will be changed to the player that did not move.
5.	Quit the current game. The command will be a ‘Q’ in the first column of the line.
6.	At this point, print the final board configuration using the same format as was used in the input. This terminates input for the current game
7.	You may assume that the commands will be syntactically correct. Put one blank line between output from separate games and no blank lines anywhere else in the output.





Team members:
1. Richa cristina :20WH1A0576:CSE
2. P Meghana :20WH1A12A8 :IT
3. K Sushmeetha:20WH1A1258 :IT
4. G Alekhya:20WH1A0432 :ECE
5. P Aruna : 20WH1A0237:ECE
6. Bushra mahaveen: 21WH5A0204: EEE


